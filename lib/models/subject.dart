// To parse this JSON data, do
//
//     final subject = subjectFromMap(jsonString);

import 'dart:convert';

List<Subject> subjectFromMap(String str) =>
    List<Subject>.from(json.decode(str).map((x) => Subject.fromMap(x)));

String subjectToMap(List<Subject> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Subject {
  Subject({
    this.name,
    this.id,
    this.description,
    this.icon,
    this.color,
    this.color2,
  });

  String name;
  int id;
  String description;
  String icon;
  String color;
  String color2;

  Subject copyWith({
    String name,
    int id,
    String description,
    String icon,
    String color,
    String color2,
  }) =>
      Subject(
        name: name ?? this.name,
        id: id ?? this.id,
        description: description ?? this.description,
        icon: icon ?? this.icon,
        color: color ?? this.color,
        color2: color2 ?? this.color2
      );

  factory Subject.fromMap(Map<String, dynamic> json) => Subject(
        name: json["name"],
        id: json["id"],
        description: json["description"],
        icon: json["icon"],
        color: json["color"],
        color2: json["color2"]
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "id": id,
        "description": description,
        "icon": icon,
        "color": color,
        "color2": color2,
      };
}
