import 'dart:async';

import 'package:Sumedha/UI/common/themes/base_theme.dart';
import 'package:Sumedha/UI/screens/auth/login_screen.dart';
import 'package:Sumedha/UI/screens/home/home_screen.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:Sumedha/providers/bottom_navbar_provider.dart';
import 'package:Sumedha/providers/exam_provider.dart';
import 'package:Sumedha/providers/feeds_provider.dart';
import 'package:Sumedha/providers/profile_provider.dart';
import 'package:Sumedha/providers/progress_provider.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/providers/subject_provider.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sentry/sentry.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sentry = SentryClient(
    dsn:
        "https://761766aacaf1457494e42accfa659a56@o381344.ingest.sentry.io/5500850");

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runZonedGuarded(
      () => runApp(MyApp()),
      (error, stackTrace) async {
        await sentry.captureException(
          exception: error,
          stackTrace: stackTrace,
        );
      },
    );
  });
}

// void main() {
//   WidgetsFlutterBinding.ensureInitialized();
//   SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
//       .then((_) {
//     runApp(new MyApp());
//   });
// }

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Themer _themer = Themer(BaseTheme.lightTheme);
  String theme;

  @override
  void initState() {
    try {
      SharedPreferences.getInstance().then((prefs) {
        if (prefs.getString(ThemeStringConstants.theme) ==
            ThemeStringConstants.dark)
          _themer.setTheme(BaseTheme.darkTheme);
        else if (prefs.getString(ThemeStringConstants.theme) ==
            ThemeStringConstants.light)
          _themer.setTheme(BaseTheme.lightTheme);
        else
          _themer.setTheme(BaseTheme.lightTheme);
      });
    } catch (e) {
      print("Error Loading Theme: $e");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<Themer>(
            create: (_) => _themer,
          ),
          ChangeNotifierProvider<SubjectProvider>(
            create: (_) => SubjectProvider(),
          ),
          ChangeNotifierProvider<ExamProvider>(
            create: (_) => ExamProvider(),
          ),
          ChangeNotifierProvider<QuestionsProvider>(
            create: (_) => QuestionsProvider(),
          ),
          ChangeNotifierProvider<FeedsProvider>(
            create: (_) => FeedsProvider(),
          ),
          ChangeNotifierProvider<ProgressProvider>(
            create: (_) => ProgressProvider(),
          ),
          ChangeNotifierProvider<ProfileProvider>(
            create: (_) => ProfileProvider(),
          ),
          ChangeNotifierProvider<AuthService>(create: (_) => AuthService()),
          ChangeNotifierProvider<BottomNavigationBarProvider>(
            create: (_) => BottomNavigationBarProvider(),
          ),
        ],
        child: Consumer<Themer>(
          builder: (context, theme, _) => MaterialApp(
            themeMode: ThemeMode.system,
            debugShowCheckedModeBanner: false,
            title: 'Sumedha',
            routes: {
              // '/splash': (context) => SplashScreen(),
              '/': (context) => HomePage(),
              '/login': (context) => LoginScreen(),
            },
            initialRoute: '/',
            theme: theme.getTheme(),
          ),
        ));
  }
}
