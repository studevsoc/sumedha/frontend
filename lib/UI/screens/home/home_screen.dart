import 'package:Sumedha/UI/common/loader.dart';
import 'package:Sumedha/UI/screens/auth/logged_user_builder.dart';
import 'package:Sumedha/UI/screens/auth/login_screen.dart';
import 'package:Sumedha/providers/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<AuthService>().signInSilently();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthService>(
      builder: (context, provider, child) {
        if (provider.authStatus == AuthStatus.AUTH)
          return child;
        else if (provider.authStatus == AuthStatus.UNAUTH) {
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => LoginScreen()));
          });
          return Scaffold(
            body: Center(
              child: Loader(),
            ),
          );
        } else {
          if (provider.authStatus == AuthStatus.ERROR) {
            return Scaffold(
              body: Center(
                child: Loader(),
              ),
            );
          }
        }
        return Scaffold(
            body: Center(
                child:
                    SpinKitCubeGrid(color: Theme.of(context).iconTheme.color)));
      },
      child: LoggedUserBuilder(),
    );
  }
}
