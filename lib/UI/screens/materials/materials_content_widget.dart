import 'dart:io';

import 'package:Sumedha/UI/common/alert_dialog.dart';
import 'package:Sumedha/UI/screens/materials/youtubeplayer.dart';
import 'package:Sumedha/models/materials.dart';
import 'package:Sumedha/providers/subject_provider.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:provider/provider.dart';

class MaterialsButton extends StatefulWidget {
  const MaterialsButton({
    Key key,
    @required this.material,
  }) : super(key: key);

  final Materials material;

  @override
  _MaterialsButtonState createState() => _MaterialsButtonState();
}

class _MaterialsButtonState extends State<MaterialsButton> {
  bool isDownloaded = false, isDownloading = false;
  @override
  Widget build(BuildContext context) {
    final subjectProvider = Provider.of<SubjectProvider>(context);
    final screenSize = ScreenSize(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: screenSize.width / 6),
      child: RaisedButton(
        padding: EdgeInsets.symmetric(vertical: 20),
        onPressed: () async {
          setState(() {
            isDownloading = true;
          });
          if (widget.material.fileType == 5) {
            setState(() {
              isDownloading = false;
              isDownloaded = true;
            });
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => YoutubeViewer(widget.material.fileLink)));
            return;
          }

          Directory dir = await path.getApplicationDocumentsDirectory();
          OpenResult openResult;
          String fullPath = dir.path + '/' + widget.material.fileName;
          print(fullPath);
          bool ifExits = await checkIfExists(fullPath);
          if (ifExits) {
            setState(() {
              isDownloading = false;
              isDownloaded = true;
            });

            openResult = await OpenFile.open(fullPath).then((value) async {
              await subjectProvider.materialRead(widget.material.id.toString());
              return value;
            });
            print(openResult.message);
          } else
            await subjectProvider
                .downloadGet(url: widget.material.file, fullPath: fullPath)
                .then((value) async {
              if (value == 200) {
                setState(() {
                  isDownloading = false;
                  isDownloaded = true;
                });
                openResult = await OpenFile.open(fullPath);
              } else {
                setState(() {
                  isDownloading = false;
                  isDownloaded = false;
                });
                Widget button = FlatButton(
                  child: Text("Continue"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                );
                AppAlert.showAlertDialog(
                    context: context,
                    continueButton: button,
                    title: 'File not downloaded',
                    subtitle: 'Some error occurred');
              }
            }).catchError((e) {
              print(e);
            });
        },
        elevation: 3,
        color: Theme.of(context).colorScheme.background,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Theme.of(context).textTheme.headline1.color),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Center(
                child: Text(widget.material.name ?? ''),
              ),
            ),
            isDownloading
                ? SizedBox(
                    height: 20,
                    width: 20,
                    child: CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.green),
                    ),
                  )
                : isDownloaded
                    ? Icon(
                        Icons.check,
                        color: Colors.green,
                      )
                    : SizedBox(),
            SizedBox(
              width: 15,
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> checkIfExists(String fileName) async {
    if (await File(fileName).exists()) {
      print("File exists");
      return true;
    } else {
      print("File don't exists");
      return false;
    }
  }
}
