import 'package:Sumedha/UI/screens/materials/materials_page_widget.dart';
import 'package:Sumedha/models/subject.dart';
import 'package:Sumedha/providers/subject_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MaterialsPage extends StatefulWidget {
  @override
  _MaterialsPageState createState() => _MaterialsPageState();
}

class _MaterialsPageState extends State<MaterialsPage> {
  List<Subject> subjectList;
  @override
  Widget build(BuildContext context) {
    final subjectProvider = Provider.of<SubjectProvider>(
      context,
    );

    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          subjectProvider.refreshSubjects();
        },
        child: FutureBuilder<List<Subject>>(
          future: subjectProvider.getAllSubjects(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, int index) {
                  return MaterialsCard(
                    subject: snapshot.data[index],
                  );
                },
              );
            } else
              return Center(child: SpinKitCubeGrid(color: Theme.of(context).iconTheme.color));
          },
        ),
      ),
    );
  }
}
