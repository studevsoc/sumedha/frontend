import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/test/question_bubble.dart';
import 'package:Sumedha/UI/screens/test/question_card_widget.dart';
import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/providers/exam_provider.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class TestTakingScreen extends StatefulWidget {
  final String subjectName, testName, slug;

  final Duration testDuration;
  final int totalQuestions;
  // final Exam exam;
  const TestTakingScreen({
    Key key,
    @required this.subjectName,
    @required this.testName,
    @required this.testDuration,
    @required this.totalQuestions,
    //    @required this.exam,
    @required this.slug,
  }) : super(key: key);
  @override
  _TestTakingScreenState createState() => _TestTakingScreenState();
}

class _TestTakingScreenState extends State<TestTakingScreen> {
  bool isSubmitting = false;
  bool isWaitingForNextQuestion = false;
  bool isSubmittingTest = false;

  // Timer _timer;
  // int _start = 10;
  //
  // void startTimer() {
  //   const oneSec = const Duration(seconds: 1);
  //   _timer = new Timer.periodic(
  //     oneSec,
  //         (Timer timer) => setState(
  //           () {
  //         if (_start < 1) {
  //           timer.cancel();
  //         } else {
  //           _start = _start - 1;
  //         }
  //       },
  //     ),
  //   );
  // }
  //
  // @override
  // void dispose() {
  //   _timer.cancel();
  //   super.dispose();
  // }
  //
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   _start=widget.testDuration.inSeconds;
  //   startTimer();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final examProvider = Provider.of<ExamProvider>(context);
    return WillPopScope(
      onWillPop: () {
        showDialogBox(
            context: context,
            title: 'Exam has started',
            message: 'Finish the exam to exit');
        return;
      },
      child: Consumer<QuestionsProvider>(
        builder: (context, questionProvider, child) => FutureBuilder<Questions>(
            future: questionProvider.getAllQuestions(slug: widget.slug),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Widget> questionCards = [];
                List<Widget> questionBubbles = [];
                int count = 0;
                snapshot.data.exam.questionSet.forEach((question) {
                  count++;
                  questionBubbles.add(QuestionBubble(
                    questionNumber: count,
                    questionSet: question,
                  ));
                  questionCards.add(
                    QuestionCard(
                      questionSet: question,
                    ),
                  );
                });
                questionCards.add(SizedBox());
                questionBubbles.add(SizedBox());
                return isSubmittingTest
                    ? Scaffold(
                        // drawer: ExamAppDrawer(testName: snapshot.data.exam.name, testDescription: snapshot.data.exam.description, totalTime: questionProvider.examDuration.inMinutes, totalQns: widget.totalQuestions),
                        appBar: ExamAppBar(snapshot.data.exam.name),
                        body: Center(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'Submitting test',
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                              ),
                              SpinKitCubeGrid(color: Theme.of(context).iconTheme.color),
                            ],
                          ),
                        ),
                      )
                    : Scaffold(
                        drawer: ExamAppDrawer(testName: snapshot.data.exam.name, testDescription: snapshot.data.exam.description, totalTime: questionProvider.examDuration.inMinutes, totalQns: widget.totalQuestions),
                        appBar: ExamAppBar(snapshot.data.exam.name),
                        body: SingleChildScrollView(
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(
                                  height: 16,
                                ),

                                questionProvider.currentQuestion >
                                        questionProvider.totalQuestions
                                    ? Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Text(
                                          'All questions attended',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline5,
                                        ),
                                      )
                                    : Column(
                                        children: [
                                          Text(
                                              (questionProvider.getTotalQnsLeft())
                                                      .toString() +
                                                  ' Questions left ',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2),
                                          Text(
                                              ' Duration : ' +
                                                  questionProvider
                                                      .examDuration.inMinutes
                                                      .toString() +
                                                  ' mins',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2),
                                          Text(
                                            'Started at: ' +
                                                DateFormat('Hm', 'en_US')
                                                    .format(questionProvider
                                                        .startTime)
                                                    .toString() +
                                                ' and Ending at: ' +
                                                DateFormat('Hm', 'en_US')
                                                    .format(questionProvider
                                                        .endTime)
                                                    .toString(),
                                          ),

                                          // Text(
                                          //     ' Ends in : ' +
                                          //         Duration(seconds: _start).inMinutes
                                          //             .toString() +
                                          //         ' mins '
                                          //         +
                                          //         (Duration(seconds: _start).inSeconds % 60)
                                          //             .toString() +
                                          //         ' secs'
                                          //     ,
                                          //     style: Theme
                                          //         .of(context)
                                          //         .textTheme
                                          //         .bodyText2),

                                          // Text(Duration (seconds: _start).inMinutes.toString() + "mins" + Duration (seconds: _start).inSeconds.toString() + "secs"),
                                        ],
                                      ),
                                questionProvider.currentQuestion >
                                        questionProvider.totalQuestions
                                    ? Column(
                                        children: [
                                          SizedBox(
                                            height: 50,
                                          ),
                                          Text(
                                            'Submit test',
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1,
                                          ),
                                          RaisedButton(
                                            onPressed: () async {
                                              setState(() {
                                                isSubmittingTest = true;
                                              });

                                              await questionProvider
                                                  .endExam()
                                                  .then((value) {
                                                examProvider.refreshExams();
                                              });
                                              setState(() {
                                                isSubmittingTest = false;
                                              });

                                              // _timer.cancel();

                                              Navigator.pop(context);
                                            },
                                            child: Text('Submit'),
                                          ),
                                          SizedBox(
                                            height: 16,
                                          ),
                                          RaisedButton(
                                            child: Text('Go back'),
                                            onPressed: () {
                                              questionProvider.retryExam();
                                            },
                                          ),
                                        ],
                                      )
                                    : questionCards[
                                        questionProvider.currentQuestion],
                                (questionProvider.currentQuestion <=
                                        questionProvider.totalQuestions)
                                    ? isWaitingForNextQuestion
                                        ? SpinKitCubeGrid(color: Theme.of(context).iconTheme.color)
                                        : Column(
                                          children: [
                                            Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  GestureDetector(
                                                    onTap: () {
                                                      if (!isWaitingForNextQuestion)
                                                        questionProvider
                                                            .prevQuestion();
                                                    },
                                                    child: FaIcon(
                                                      FontAwesomeIcons.backward,
                                                      size: 35,
                                                      color: Theme.of(context)
                                                          .iconTheme
                                                          .color,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 56,
                                                  ),
                                                  GestureDetector(
                                                    child: FaIcon(
                                                      FontAwesomeIcons.timesCircle,
                                                      size: 35,
                                                      color: Colors.orange,
                                                    ),
                                                    onTap: () async{
                                                      questionProvider.uncheckAnswer(
                                                          questionID: questionProvider
                                                              .markedAnswersList[
                                                                  questionProvider
                                                                      .currentQuestion]
                                                              .questionID,
                                                          answerID: questionProvider
                                                              .markedAnswersList[
                                                                  questionProvider
                                                                      .currentQuestion]
                                                              .answerID);

                                                        if (!isWaitingForNextQuestion) {
                                                          String response = '';
                                                          setState(() {
                                                            isWaitingForNextQuestion =
                                                            true;
                                                          });

                                                          response =
                                                              await questionProvider
                                                              .submitBlankAnswer();

                                                          if (response ==
                                                              'timeout') {
                                                            await showDialogBox(
                                                                context: context,
                                                                title: 'Exam ended',
                                                                message:
                                                                'The last response is saved. Exam will end now.');

                                                            Navigator.of(context)
                                                                .pushNamedAndRemoveUntil(
                                                                '/home',
                                                                    (Route route) =>
                                                                false);
                                                            questionProvider
                                                                .endExam();
                                                          }
                                                          setState(() {
                                                            isWaitingForNextQuestion =
                                                            false;
                                                          });
                                                        }

                                                    },
                                                  ),
                                                  SizedBox(
                                                    width: 56,
                                                  ),
                                                  GestureDetector(
                                                    child: FaIcon(
                                                      FontAwesomeIcons.checkCircle,
                                                      size: 35,
                                                      color: Colors.green,
                                                    ),
                                                    onTap: () async {
                                                      if (questionProvider
                                                              .markedAnswersList[
                                                                  questionProvider
                                                                      .currentQuestion]
                                                              .selectedOption >
                                                          -1) {
                                                        if (!isWaitingForNextQuestion) {
                                                          String response = '';
                                                          setState(() {
                                                            isWaitingForNextQuestion =
                                                                true;
                                                          });

                                                          response =
                                                              await questionProvider
                                                                  .submitCurrentAnswer();

                                                          if (response ==
                                                              'timeout') {
                                                            await showDialogBox(
                                                                context: context,
                                                                title: 'Exam ended',
                                                                message:
                                                                    'The last response is saved. Exam will end now.');

                                                            Navigator.of(context)
                                                                .pushNamedAndRemoveUntil(
                                                                    '/home',
                                                                    (Route route) =>
                                                                        false);
                                                            questionProvider
                                                                .endExam();
                                                          }
                                                          setState(() {
                                                            isWaitingForNextQuestion =
                                                                false;
                                                          });
                                                        }
                                                      }
                                                    },
                                                  ),
                                                  SizedBox(
                                                    width: 56,
                                                  ),
                                                  GestureDetector(
                                                    onTap: () async {
                                                      questionProvider
                                                          .nextQuestion();
                                                    },
                                                    child: FaIcon(
                                                      FontAwesomeIcons.forward,
                                                      size: 35,
                                                      color: Theme.of(context)
                                                          .iconTheme
                                                          .color,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            SizedBox(
                                              height: 16,
                                            ),
                                            RaisedButton(
                                              child: Text('Mark & Submit'),
                                              onPressed: () async {
                                                if (questionProvider
                                                    .markedAnswersList[
                                                questionProvider
                                                    .currentQuestion]
                                                    .selectedOption >
                                                    -1) {
                                                  if (!isWaitingForNextQuestion) {
                                                    String response = '';
                                                    setState(() {
                                                      isWaitingForNextQuestion =
                                                      true;
                                                    });

                                                    response =
                                                    await questionProvider
                                                        .markAndSubmit();

                                                    if (response ==
                                                        'timeout') {
                                                      await showDialogBox(
                                                          context: context,
                                                          title: 'Exam ended',
                                                          message:
                                                          'The last response is saved. Exam will end now.');

                                                      Navigator.of(context)
                                                          .pushNamedAndRemoveUntil(
                                                          '/home',
                                                              (Route route) =>
                                                          false);
                                                      questionProvider
                                                          .endExam();
                                                    }
                                                    setState(() {
                                                      isWaitingForNextQuestion =
                                                      false;
                                                    });
                                                  }
                                                }
                                              },
                                            ),
                                          ],
                                        )
                                    : SizedBox(),
                                isSubmitting
                                    ? SpinKitCubeGrid(color: Theme.of(context).iconTheme.color)
                                    : SizedBox(),
                              ],
                            ),
                          ),
                        ),
                        bottomNavigationBar: questionProvider.currentQuestion >
                                questionProvider.totalQuestions
                            ? SizedBox()
                            : SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8, bottom: 8),
                                  child: Row(
                                    children: questionBubbles,
                                  ),
                                ),
                              ),
                      );
              } else {
                return Scaffold(
                  drawer: IgnorePointer(child: AppDrawer()),
                  appBar: SumedhaAppBar(
                    ignoreTouches: true,
                  ),
                  body: Center(
                    child: SpinKitCubeGrid(color: Theme.of(context).iconTheme.color),
                  ),
                );
              }
            }),
      ),
    );
  }

  showDialogBox(
      {@required BuildContext context,
      @required String title,
      @required String message}) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
          );
        }).then((value) {});
  }
}
