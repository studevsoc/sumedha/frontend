import 'package:Sumedha/UI/screens/test/test_completed_widget.dart';
import 'package:flutter/material.dart';

class TestCompleted extends StatefulWidget {
  @override
  _TestCompletedState createState() => _TestCompletedState();
}

class _TestCompletedState extends State<TestCompleted> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TestCompletedWidget(),
    );
  }
}
