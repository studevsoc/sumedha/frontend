import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/components/bottom_navbar.dart';
import 'package:Sumedha/providers/bottom_navbar_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoggedUserBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<BottomNavigationBarProvider>(
        builder: (context, bottomBarProvider, _) {
      return Scaffold(
        drawer: AppDrawer(),
        appBar: SumedhaAppBar(),
        body: bottomBarProvider.tabItems[bottomBarProvider.currentIndex],
        bottomNavigationBar: BottomNavBar(),
      );
    });
  }
}
