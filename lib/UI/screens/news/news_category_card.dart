import 'package:Sumedha/UI/screens/news/news_des.dart';
import 'package:Sumedha/models/feed.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NewsCategoryCard extends StatelessWidget {
  const NewsCategoryCard({
    @required this.feed,
  });
  final FeedsList feed;

  @override
  Widget build(BuildContext context) {
    final screenSize = ScreenSize(context);
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => NewsDescription(
                    feed: feed,
                  ))),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.black, width: 1.5),
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Container(
            height: screenSize.height / 9,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20),
                  child: FaIcon(
                    FontAwesomeIcons.globe,
                    size: 30,
                    color: Colors.blueAccent,
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.fromLTRB(25, 8, 20, 0),
                        child: Text(
                          feed.name,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.fromLTRB(25, 5, 20, 10),
                        child: Text(
                          'Read news on ' + feed.name,
                          style: Theme.of(context).textTheme.subtitle2,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
