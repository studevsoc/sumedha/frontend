import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/news/news_category_card.dart';
import 'package:Sumedha/models/feed.dart';
import 'package:Sumedha/providers/feeds_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NewsListHome extends StatefulWidget {
  @override
  _NewsListHomeState createState() => _NewsListHomeState();
}

class _NewsListHomeState extends State<NewsListHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SumedhaAppBar(),
      body: NewsList(),
    );
  }
}

class NewsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final feeds = Provider.of<FeedsProvider>(context);
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 8),
            child: Center(
              child: Text(
                'Headlines',
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
          ),
          FutureBuilder<List<FeedsList>>(
              future: feeds.getAllFeeds(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0)
                    return Center(
                      child: Text('No news found'),
                    );
                  return Flexible(
                    child: ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) => NewsCategoryCard(
                              feed: snapshot.data[index],
                            )),
                  );
                } else
                  return SpinKitCubeGrid(color: Theme.of(context).iconTheme.color);
              }),
        ],
      ),
    );
  }
}
