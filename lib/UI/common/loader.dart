import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 300,
            child: Image.asset(
              LogoStringConstants.kiteslogo,
              fit: BoxFit.fitHeight,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          SpinKitCubeGrid(color: Theme.of(context).iconTheme.color),
        ],
      ),
    );
  }
}
