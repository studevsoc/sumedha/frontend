import 'package:flutter/material.dart';

class ColorConstants {
  static const Color deepPink = Color(0xffAB0346);
  static const Color deepViolet = Color(0xff800983);
  static const Color deepOrange = Color(0xffC55151);
  static const Color deepRed = Color(0xffD63333);
  static const Color deepBlue = Color(0xff2E0F53);
  static const Color black = Colors.black;
  static const Color white = Color(0xffF6F6F6);
  static const Color primaryRed = Color(0xffcf3535);
  static const Color primaryBlack = Color(0xff1f1c24);

  // Light Theme Color Scheme

  static final ColorScheme lightColors = ColorScheme.light().copyWith(
    primaryVariant: primaryBlack,
    secondaryVariant: primaryRed,
  );

// Dark Theme Color Scheme

  static final ColorScheme darkColors = ColorScheme.dark().copyWith();
}
