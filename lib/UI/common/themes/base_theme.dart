import 'package:Sumedha/UI/common/themes/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'color_constants.dart';

class BaseTheme {
// Light Theme

  static ThemeData lightTheme = ThemeData.light().copyWith(
      primaryColor: ColorConstants.primaryBlack,
      colorScheme: ColorConstants.lightColors,
      scaffoldBackgroundColor: ColorConstants.white,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      appBarTheme: AppBarTheme(
          elevation: 1,
          color: ColorConstants.white,
          iconTheme: IconThemeData(color: Colors.black)),
      textTheme: GoogleFonts.cantarellTextTheme(TextThemes.lightTextTheme),
      primaryTextTheme:
          GoogleFonts.cantarellTextTheme(TextThemes.lightPrimaryTextTheme));

// Dark Theme

  static ThemeData darkTheme = ThemeData.dark().copyWith(
      colorScheme: ColorConstants.darkColors,
      appBarTheme: AppBarTheme(
          elevation: 1,
          color: ThemeData.dark().scaffoldBackgroundColor,
          iconTheme: IconThemeData(color: Colors.white)),
      visualDensity: VisualDensity.adaptivePlatformDensity,
      textTheme: GoogleFonts.cantarellTextTheme(TextThemes.darkTextTheme),
      primaryTextTheme:
          GoogleFonts.cantarellTextTheme(TextThemes.darkPrimaryTextTheme));
}
