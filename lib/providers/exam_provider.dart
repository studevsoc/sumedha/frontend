import 'package:Sumedha/models/exams.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';

class ExamProvider extends ChangeNotifier {
  final DataSource _dataSource = DataSource();
  List<Exam> examsList;
  List<StudentExam> studentExamList;
  StorageHelper _storageHelper = StorageHelper();

  Future<List<Exam>> getAllExams() async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    this.examsList = await _dataSource.getAllExams(access);

    return examsList;
  }

  void refreshExams() {
    this.examsList = null;
    this.studentExamList = null;
    notifyListeners();
  }

  Future<List<StudentExam>> getStudentExams() async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    this.studentExamList = await _dataSource.getStudentExams(access: access);

    return studentExamList;
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }
}
