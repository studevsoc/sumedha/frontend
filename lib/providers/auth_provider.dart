import 'dart:convert';

import 'package:Sumedha/models/user.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/foundation.dart';

enum AuthStatus { AUTH, UNAUTH, ERROR, LOADING }

class AuthService extends ChangeNotifier {
  AuthStatus _authStatus = AuthStatus.UNAUTH;
  AuthStatus get authStatus => _authStatus;

  User user;
  String userData;
  String username, password;
  bool autoLogin = false,
      tokenFound = false,
      loginDataFound = false,
      loggedIn = false;
  final DataSource _dataSource = DataSource();
  final StorageHelper _storageHelper = StorageHelper();

  Future<void> loginUser(
      {@required String username, @required String password}) async {
    _authStatus = AuthStatus.LOADING;
    notifyListeners();
    User tempUser;
    tempUser = await _dataSource.login(username: username, password: password);
    if (tempUser == null) {
      _authStatus = AuthStatus.ERROR;
      notifyListeners();
      Future.delayed(const Duration(seconds: 1), () {
        _authStatus = AuthStatus.UNAUTH;
        notifyListeners();
      });
    } else {
      this.user = tempUser;
      await saveTokenDataToLocal();
      this.username = username;
      this.password = password;
      await saveLoginDataToLocal();

      this.loggedIn = true;
      await _storageHelper.saveString(key: 'auto_login', value: 'true');
      _authStatus = AuthStatus.AUTH;
      notifyListeners();
    }
    return;
  }

  Future<void> saveTokenDataToLocal() async {
    try {
      await _storageHelper.saveMapData(
          key: AuthStringConstants.userData, data: user.res);
      await _storageHelper.saveString(
          key: AuthStringConstants.access, value: user.access);
      await _storageHelper.saveString(
          key: AuthStringConstants.refresh, value: user.refresh);
    } catch (e) {
      print(e);
    }
  }

  Future<void> refreshUser() async {
    if (DateTime.now().isAfter(user.exp)) {
      User tempUser = await _dataSource.refresh(user.refresh);
      if (tempUser != null) {
        this.user = tempUser;
        await saveTokenDataToLocal();
      } else {
        _authStatus = AuthStatus.ERROR;
        notifyListeners();
        Future.delayed(const Duration(seconds: 1), () {
          _authStatus = AuthStatus.UNAUTH;
          notifyListeners();
        });
      }
    }
  }

  Future<User> getUserDataFromToken() async {
    this.userData =
        await _storageHelper.getString(AuthStringConstants.userData) ?? '';

    if (userData == '') {
      return null;
    }
    User tempUser = User.fromMap(jsonDecode(userData));
    this.user = tempUser;
    this.tokenFound = true;
    return tempUser;
  }

  Future<bool> checkIfLoginDataSaved() async {
    try {
      var username =
          await _storageHelper.getString(AuthStringConstants.username);
      var password =
          await _storageHelper.getString(AuthStringConstants.password);
      if (username == null || password == null) {
        this.loginDataFound = false;
        return false;
      } else {
        this.password = password;
        this.username = username;
        this.loginDataFound = true;
        return true;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> checkIfUserDataSaved() async {
    try {
      final String s =
          await _storageHelper.getString(AuthStringConstants.userData);
      if (s == null) {
        this.tokenFound = false;
        return false;
      } else {
        this.tokenFound = true;
        return true;
      }
    } catch (e) {
      return false;
    }
  }

  Future<void> signInSilently() async {
    _authStatus = AuthStatus.LOADING;
    notifyListeners();
    bool userDataFound = await checkIfUserDataSaved() ?? false;
    bool loginDataFound = await checkIfLoginDataSaved() ?? false;

    if (userDataFound && loginDataFound) {
      await loginUser(username: username, password: password);
      if (user.exp.isBefore(DateTime.now())) {
        await refreshUser();
        notifyListeners();
      }
    } else {
      _authStatus = AuthStatus.UNAUTH;
      notifyListeners();
    }
  }

  Future<void> signOut() async {
    this.user = null;
    await _storageHelper.storage.delete(key: AuthStringConstants.userData);
    await _storageHelper.storage.delete(key: AuthStringConstants.username);
    await _storageHelper.storage.delete(key: AuthStringConstants.password);
    await _storageHelper.saveString(key: 'auto_login', value: 'false');
    _authStatus = AuthStatus.UNAUTH;
    notifyListeners();
  }

  Future<void> saveLoginDataToLocal() async {
    try {
      await _storageHelper.saveString(
          key: AuthStringConstants.username, value: username);
      await _storageHelper.saveString(
          key: AuthStringConstants.password, value: password);
    } catch (e) {
      print(e);
    }
  }

  Future<void> checkAutoLogin() async {
    String s = await _storageHelper.getString('auto_login') ?? 'false';
    if (s == 'true') {
      this.autoLogin = true;
    } else {
      this.autoLogin = false;
    }
  }
}
