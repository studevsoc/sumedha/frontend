enum AppTheme { Light, Dark }

enum Subjects { maths, physics, chemistry }

enum Tab { home, test, material, schedule }

enum AnswerState { answered, marked, skipped, unAttended }
