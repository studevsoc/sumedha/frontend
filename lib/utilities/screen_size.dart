import 'package:flutter/material.dart';

class ScreenSize {
  double height, width;

  ScreenSize(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    this.height = size.height;
    this.width = size.width;
  }

  ScreenSize getSize(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    this.height = size.height;
    this.width = size.width;
    return this;
  }
}
